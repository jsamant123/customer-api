Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :customers, only: %i[index create destory] do
        collection do
          post 'delete_me', to: 'customers#delete'
        end
      end
      #get auth token
      post '/auth/token', to: 'authentication#fetch_token'
    end
  end

  #route other urls to not_found
  get '/*a', to: 'application#not_found'
end
