# Customer API
----------------

## Requirement

1. Create a Rails 5.x API application.
2. Create an Address model with a street, city and zip_code.
3. Create a Customer model with a name and an address_id association.
4. Add an API endpoint to fetch all customers with their addresses and have the ability to filter by customer name and address street.
5. Add an API endpoint to create and destroy customers.
6. Authenticate API endpoints with a secret key.
7. Write tests if applicable

## Environment Configuration

 - Rails 5.2.2

 - Ruby 2.5.1p57 (2018-03-29 revision 63029)

 - Postgres (PostgreSQL) 11.0

## APIS

The postman suite collection url

  - https://documenter.getpostman.com/view/1671726/S11DVMyL

Details of API

   - Create Customer with address details
   - Filter Customer details by name and street address
   - Delete Customer ( token required )
   - JWT token to authorize APIs ( delete and filter customers )

APIS and USAGE

Asumming my rails app is running on port 3000 at localhost

----------

Create a customer

```ruby
    url: 'localhost:3000/api/v1/customers'
    action: 'create'
    http_verb: post
    params: {
      name: 'admin1221'
      street: 'slivelsa'
      city: 'NY'
      zip_code: '12345'
    }

    response:
    status: 200
    body: {
        "id": "c7f9a93c-72ff-41f7-a956-a295f9d5ef71",
        "name": "admin1221",
        "customer_address": {
            "id": "7b7975a7-2d54-4ff9-94fe-47f04610dbc6",
            "street": "slivelsa",
            "city": "dsfdsf",
            "zip_code": "12345"
        }
    }
```

Get Auth Token

```ruby
    url: 'localhost:3000/api/v1/auth/token'
    http_verb: post
    params: {
      name: 'admin1221'
    }

    response:
    status: 200
    body: {
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lcl9pZCI6ImRhZWM4YjEzLWUwYjMtNDlhMy1hOGE1LTUyNTFhZDBmZmEzOCIsImV4cCI6MTU1MDc1ODA1OX0.mzsdPnaPoKclbQZrdcFNz_C8OuI0qkWftCn8Gu50DiY",
    "exp": "02-21-2019 19:37",
    "name": "admin"
}
```

Filter customer results

```ruby
    url: 'localhost:3000/api/v1/customers?name=&street=sl'
    http_verb: get
    headers: { 'Authorization': 'TOKEN_FROM_GET_AUTH_TOKEN_API' }
    response:
    status: 200
    body: [
    {
        "id": "bdfa8289-eeef-44f0-a6f7-a92b38af4b5c",
        "name": "admin",
        "customer_address": {
            "id": "f2349746-8628-42cf-98a1-32290941017e",
            "street": "slivelsa",
            "city": "dsfdsf",
            "zip_code": "12345"
        }
    },
    {
        "id": "daec8b13-e0b3-49a3-a8a5-5251ad0ffa38",
        "name": "admin",
        "customer_address": {
            "id": "75669f24-faf1-4061-b2f6-28b63ea45413",
            "street": "slivelsa",
            "city": "dsfdsf",
            "zip_code": "12345"
        }
    }
]
```

Delete Customer

```ruby
    url: 'localhost:3000/api/v1/customers/delete_me'
    http_verb: post
    headers: { 'Authorization': 'TOKEN_FROM_GET_AUTH_TOKEN_API' }
    response:
    status: 200
    body: {
    "message": "deleted"
}
```

----------

Future enhancements

   - Current version of API's is V1, So to extend the application we can add more versions based on the requirements and add new level of APIs not interfering older ones
   - Customer can create only one address since the APIs are designed as per the requirement given. It can be enhanced so that user can build more addresses based on type eg: office , home etc
   - There is no edit APIs implemented, we can have a new version of API where edit functionality can be improved
   - Authentication process can be made more Streamlined as we can introduce profile of the customer where all the personal details can be stored
   - We can have a sign up process and assign some roles and assign some specific APIs restricted to the normal users.

Test CASES:

  - Due to paucity of time, I could only cover the below test cases. If you like the overall approach, I can work on the other test cases and re-submit.


Note

  - ZIP code validations are considered only US format as of now
  - Usually customer_id is placed in Address table so that a customer has one address, but the current relation is where customer has many addresses
