require 'rails_helper'

RSpec.describe Api::V1::CustomersController, type: :controller do

  context 'when is successfully created' do
    describe "POST #create" do
      before do
        post :create, params: {
          name: 'customer name',
          street: 'stree_name',
          city: 'city_name',
          zip_code: '12345'
        }
      end
      it "returns http success" do
        expect(response).to have_http_status(:success)
      end
      it "JSON body response contains expected customer and address attributes" do
        json_response = JSON.parse(response.body)
        expect(json_response.keys).to match_array(["customer_address", "id", "name"])
      end
    end
  end
  context 'when failed' do
    describe "POST #create" do
      before do
        post :create, params: {
          name: 'customer name',
          street: 'stree_name',
          city: 'city_name',
          zip_code: ''
        }
      end
      it "returns http success" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
      it "JSON body response contains error of zip_code" do
        json_response = JSON.parse(response.body)
        expect(json_response).to eql("zip_code"=>["can't be blank", "should be 12345 or 12345-1234"])
      end
    end
  end
end
