FactoryBot.define do
  factory :customer do
    sequence(:name) { |n| "Customer#{n}" } # Unique name for every customer
    association :address, factory: :address, strategy: :build
  end
end
