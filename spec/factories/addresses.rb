FactoryBot.define do
  factory :address do
    sequence(:street) { |n| "Street#{n}" } # Unique street for every customer
    sequence(:city) { |n| "City#{n}" } # Unique name for every customer
    sequence(:zip_code) { |n| "1234#{n}" } # Unique name for every customer
  end
end
