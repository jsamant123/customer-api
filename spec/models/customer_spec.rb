require 'rails_helper'

RSpec.describe Customer, type: :model do
  subject { FactoryBot.create(:customer) }
  describe 'associations' do
    it { should respond_to(:address) }
  end

 it { should validate_presence_of(:name) }
end
