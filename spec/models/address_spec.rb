require 'rails_helper'

RSpec.describe Address, type: :model do
  describe 'associations' do
    it { should have_many(:customers) }
  end
  it { should validate_presence_of(:street) }
  it { should validate_presence_of(:city) }
  it { should validate_presence_of(:zip_code) }
end
