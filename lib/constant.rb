# frozen_string_literal: true

class Constant
  #pagination
  CUSTOMER_LIMIT = 10

  #regex
  ZIP_CODE_REGEX = /\A\d{5}-\d{4}|\A\d{5}\z/
end
