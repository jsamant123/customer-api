class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers, id: :uuid do |t|
      t.string :name
      t.uuid :address_id, foreign_key: true

      t.timestamps
    end
    add_index :customers, :name
  end
end
