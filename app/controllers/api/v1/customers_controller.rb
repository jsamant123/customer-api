class Api::V1::CustomersController < ApplicationController
  before_action :authorize_request, except: %i[create]

  # GET /customers
  def index
    @customers = Customer.fetch_customers(filter_params)
    render json: @customers
  end

  # POST /customers #customer with address creation
  def create
    @address = Address.new(address_params)
    @address.customers.build(customer_params)

    if @address.save
      # since the association is reverse, we have to get the customer of the address
      @customer = @address.customers.first
      render json: @customer, status: :created
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  def delete
    if @current_customer.destroy
      render json: { message: 'deleted' }, status: :ok
    else
      render json: @current_customer.errors, status: :unprocessable_entity
    end
  end

  private

  def filter_params
    params.permit(:name, :street, :page)
  end

  # Only allow a trusted parameter "white list" through.
  def customer_params
    params.permit(:name)
  end

  def address_params
    params.permit(:street, :city, :zip_code)
  end
end
