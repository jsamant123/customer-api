class Api::V1::AuthenticationController < ApplicationController
  # POST /auth/fetch_token
  def fetch_token
    @customer = Customer.find_by_name(auth_params[:name])
    if @customer
      token = JsonToken.encode(customer_id: @customer.id)
      time = Time.now + 24.hours.to_i
      render json: {
        token: token,
        exp: time.strftime('%m-%d-%Y %H:%M'),
        name: @customer.name
      }, status: :ok
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

  private

  def auth_params
    params.permit(:name)
  end
end
