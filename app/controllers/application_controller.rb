class ApplicationController < ActionController::API
  def not_found
    render json: { error: 'not_found' }
  end

  # request auth token to access the apis
  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonToken.decode(header)
      return render json: {
        errors: 'unauthorized'
      }, status: :unauthorized if @decoded.nil?

      @current_customer = Customer.find(@decoded[:customer_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end
end
