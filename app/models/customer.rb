class Customer < ApplicationRecord
  include CustomerConcern

  # validations
  validates :name, presence: true

  # associations
  belongs_to :address, dependent: :destroy

  # Filter customers and adresses by params
  def self.fetch_customers(params)
    customers = where(ilike(param_name: 'name', param_value: params[:name])) if params[:name]
    customers = customers.merge(
      Address.where(ilike(param_name: 'street', param_value: params[:street]))
    ) if params[:street]
    customers = customers.eager_load(:address).order(:name)
    customers.paginate(per_page: Constant::CUSTOMER_LIMIT, page: params[:page])
  end
end
