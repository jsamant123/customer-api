module CustomerConcern
  module ClassMethods
    def ilike(param_name:, param_value:)
      "#{param_name} ILIKE '%#{param_value}%'"
    end
  end

  module InstanceMethods; end

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end
end
