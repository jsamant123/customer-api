class Address < ApplicationRecord

  #associations
  has_many :customers

  #validations
  validates :street, :city, :zip_code, presence: true
  validates_format_of :zip_code,
                      with: Constant::ZIP_CODE_REGEX,
                      message: I18n.t('validations.address.zip_code')
end
