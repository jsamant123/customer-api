# frozen_string_literal: true

json.array! @customers do |customer|
  json.id customer.id
  json.customer_name customer.name

  json.address do
    json.street customer.address.street
    json.city customer.address.city
    json.zip_code customer.address.zip_code
  end
end
