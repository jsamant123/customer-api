class Api::V1::CustomerSerializer < ActiveModel::Serializer
  attributes :id, :name, :customer_address

  def customer_address
    Api::V1::AddressSerializer.new(object.address)
  end
end
