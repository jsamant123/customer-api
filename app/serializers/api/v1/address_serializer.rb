class Api::V1::AddressSerializer < ActiveModel::Serializer
  attributes :id, :street, :city, :zip_code
end
